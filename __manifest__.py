# -*- coding: utf-8 -*-
{
    'name': "contact_travel",

    'summary': """
        Module de Gestion des voyages utilisateur odoo """,

    'description': """
        Le module doit répondre aux besoins internes de weasydoo, pour 
        faciliter la gestion des voyages utilisateurs
    """,

    'author': "Chaachoua Abdelbaki",

    'category': 'Weasydoo',
    'version': '1.17.0.0',
    'website': 'www.weasydoo.com',



    # any module necessary for this one to work correctly
    'depends': ['base', 'contacts'],

    # always loaded
    'data': [

        'security/ir.model.access.csv',
        'views/voyage_views.xml',
        'views/menuitem.xml',
        'views/inherited_contact_form.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        # 'demo/demo.xml',
        # 'demo/voyage.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
