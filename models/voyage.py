# -*- coding: utf-8 -*-

from odoo import models, fields, api


class voyage(models.Model):
    """   Based Model for user travel management """
    _name = "voyage"
    _inherit = ['mail.thread']
    _description = "Voyage"
    _order = "date_de_depart"
    #  travel record name
    _rec_name = 'nom'

    nom = fields.Char(string='Nom', store=True)
    date_de_depart = fields.Date(string="Date de Départ", store=True)
    country_id = fields.Many2one(comodel_name='res.country', string='Country')
    destination = fields.Many2one(comodel_name='res.country.state', string='State',
                                  domain="[('country_id', '=', country_id)]")

    contact = fields.Many2one('res.partner', string='Contact')

    amount = fields.Float(string='Montant', store=True)