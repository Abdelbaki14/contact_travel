# -*- coding: utf-8 -*-

from odoo import models, fields, api


class res_partner(models.Model):
    """ Inherit contact partner model for adding contact related travels as smart button
    and adding a method for rewrad level management based on user's travel total amount

    """

    #: To be compliant with the res.partner model
    _name = 'res.partner'
    #: To inherits explicitly variables and methods from the res.partner model
    _inherit = ['res.partner', 'mail.thread']

    # The different levels  defined for user's travel rewards
    reward_levels = [('argent', 'Argent'), ('or', 'Or'), ('platine', 'Platine')]

    rewrad_level = fields.Selection(reward_levels, 'Niveaux de Récompense',
                                    default='argent',compute='_culculate_reward_level',inverse='_inverse_reward_level')

    voyages_contact_ids = fields.One2many('voyage', 'contact',
                                          string='Voyages contacts')
    # This field is used for difine contact reward level based on total travel amount
    travel_total_amount = fields.Float(string='Montant Total des voyages')
    # This field is used for display the number of contact related travels
    travel_count = fields.Integer(compute='_travel_count', string='Voyages')

    @api.depends('voyages_contact_ids')
    def _travel_count(self):
        """
        Method used to calculate the number of travels of user

        """
        self.travel_count = len(self.voyages_contact_ids.ids)

    def action_button_voyages(self):
        """
        smart button action method to display user related travels
        """
        self.ensure_one()
        action = self.env.ref('contact_travel.action_voyage')
        return {

            'name': action.name,
            'type': action.type,
            'view_mode': action.view_mode,
            'res_model': action.res_model,
            'domain': [('id', 'in', self.voyages_contact_ids.ids)]

        }

    @api.depends('voyages_contact_ids.amount', 'travel_total_amount')
    def _culculate_reward_level(self):

        """
        This Method is used for auto change  the reward level based on total amount of
        user's travel
        :param self: partner recordset
        :return:
        """

        total_amount = 0
        for partner in self:
            for voyage in partner.voyages_contact_ids:
                total_amount += voyage.amount
            partner.travel_total_amount = total_amount
            if partner.travel_total_amount == 50000:
                partner.rewrad_level = 'or'
            elif partner.travel_total_amount > 50000:
                partner.rewrad_level = 'platine'
            else:
                partner.rewrad_level = 'argent'


    def _inverse_reward_level(self):
        """
        this method is added for making the computed reward level editable
        """
        total_amount = 0
        for partner in self:
            for voyage in partner.voyages_contact_ids:
                total_amount += voyage.amount
            partner.travel_total_amount = total_amount
            if partner.travel_total_amount == 50000:
                partner.rewrad_level = 'or'
            elif partner.travel_total_amount > 50000:
                partner.rewrad_level = 'platine'
            else:
                partner.rewrad_level = 'argent'
